**Idées de User Stories**

- En tant qu’utilisateur non inscrit je veux créer un compte afin de réserver des places de train pour un voyage et de faciliter mes réservations futures.
- En tant qu’utilisateur inscrit, je veux me connecter afin d’accéder aux informations relatives à mon compte et à l’historique de mes voyages.
- En tant qu’utilisateur je veux consulter les horaires des trains afin de choisir les trains qui me conviennent le mieux.
- En tant qu’utilisateur, je veux choisir les dates ainsi que les lieux de départ et d’arrivée de mon voyage afin de déterminer une liste de train qui respectent au mieux mes choix.
- En tant qu’utilisateur, je veux définir tous les participants à ce voyage afin qu’ils partent tous en même temps et qu’ils aient des places proches les unes des autres dans les wagons.
- En tant qu’utilisateur, je veux choisir les conditions du voyage telles que la classe des wagons afin de limiter la liste des trains à ceux qui respectent mes conditions.
- En tant qu’utilisateur, je veux réserver des places de train pour un voyage.

**Backlog ordonné**

*US techniques*

- En tant que développeur, je veux disposer d’un environnement de programmation qui intègre des outils de travail collaboratif afin de pouvoir partager mes programmes avec les autres développeurs.
- En tant que développeur, je veux disposer d’un environnement de test afin de pouvoir tester le code que je développe.
- En tant que membre de l’équipe de projet, je veux utiliser des outils de gestion de projet afin d’optimiser mon travail, de mieux m’impliquer et mieux collaborer au projet.

*US fonctionnelles*

On ordonne celles au dessus

*Mise en production*

- En tant qu’administrateur de l’application, je veux disposer d’un environnement de pré-production pour que les utilisateurs puissent valider l’application.
- En tant que développeur, je veux corriger des bugs apparus lors de l’utilisation du logiciel en pré-production afin d’améliorer le service rendu aux utilisateurs.


